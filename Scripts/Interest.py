class Interest:
    name = 'unnamed'
    tags = []

    def __init__(self, name, tags):
        self.name = name
        self.tags = tags

class GeneralInterest:
    name = 'unnamed'
    tags = []
    sub_interests = []

    def __init__(self, name, tags, sub_interests):
        self.name = name
        self.tags = tags
        self.sub_interests = sub_interests
