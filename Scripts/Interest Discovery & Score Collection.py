import Interest as inter
from itertools import product
import random
import numpy as np

ib_count = 4 #number of possible interest blocks for each general interest
gi_count = 4 #number of general interests

def rec_interests_score (general_interests): # recommend interests based on score
    avrg = 0
    minVal = 1e18
    maxVal =-1e18

    percentages = scores

    for i in general_interests:
        avrg = avrg + scores[i]
        minVal = min(minVal, scores[i])
        maxVal = max(maxVal, scores[i])

    avrg = avrg/len(general_interests)

    for i in percentages.keys():
        percentages[i] = (scores[i] - minVal + 1) / maxVal
        print((i, percentages[i]))

    recommended = []
    for i in general_interests:
        sub_size= len(i.sub_interests)
        amount_to_take = percentages[i] * sub_size
        for j in i.sub_interests:
            if amount_to_take > 0:
                recommended.append(j)
                amount_to_take = amount_to_take - 1
            else:
                break
    return recommended

def user_interaction (): # user interacts with the interest blocks
    for i, j in zip(scores.keys(), scores.values()):
        scores[i] = j+int(random.random()*10)

def build_mock_dataset(): # Build a mock powerset 'dataset'
    jj = 0
    ii = 0
    x = [i for i in product(range(ib_count), repeat=gi_count)]
    x = np.array(x)
    print ('finished')
    return x

#Create some interests

interests_list = [
    #Interest(name, tags)

    #Science
    inter.Interest('biology', ['memorization', 'science']), #interests_list[0]
    inter.Interest('space', ['science', 'memorization']), #1
    inter.Interest('computer_science', ['creativity', 'science', 'computers']), #2

    #Art
    inter.Interest('painting', ['creativity', 'art', 'painting']), #3
    inter.Interest('music', ['creativity', 'art', 'music']), #4
    inter.Interest('computer_art', ['creativity', 'art', 'computers']), #5

    #Sports
    inter.Interest('football', ['sports', 'ball_sports', 'leg_sports']), #6
    inter.Interest('basketball', ['sports', 'ball_sports', 'hand_sports']), #7
    inter.Interest('karate', ['sports', 'marshal_arts', 'art']), #8
    inter.Interest('e_sports', ['sports', 'computers']), #9

    #Zoology
    inter.Interest('wolves', ['animals', 'science', 'ground_animals']), # 10
    inter.Interest('birds', ['animals', 'science', 'flying_animals']), # 11
    inter.Interest('giraffes', ['animals', 'science', 'ground_animals']), #12

]

general_interests_list = [
    inter.GeneralInterest('science', ['creativity'], [interests_list[0], interests_list[1], interests_list[2]]), #0
    inter.GeneralInterest('art', ['creativity', 'art'], [interests_list[3], interests_list[4], interests_list[5]]), #1
    inter.GeneralInterest('sports', ['art', 'creativity', 'music'], [interests_list[6], [interests_list[7]], interests_list[8], interests_list[9]]), #2
    inter.GeneralInterest('zoology', ['science'], [interests_list[10], interests_list[11], interests_list[12]]), #3
]

#Initilize scores

scores = {}

for i in range(len(general_interests_list)):
    scores[general_interests_list[i]] = 1

#User interaction

user_interaction()
for i in scores.values():
    print(i)

#Recommend some interests based on score
print (scores.keys())
print (rec_interests_score(general_interests_list))

#Build up a mock dataset of users
np.savetxt("ada_user_scores_powerset.csv", build_mock_dataset(), delimiter=",")
